
# bot.py
import os

import discord
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()

@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')

client.run(token)

def handle_message(message):
    """Scans messages in text channels of a server and class the raider.io APO
       when a defined prefix is used.

    Args:
        message: Message object recived by discord bot.

    Returns:
        Embed object if the message's prefix was a command, else none
    """